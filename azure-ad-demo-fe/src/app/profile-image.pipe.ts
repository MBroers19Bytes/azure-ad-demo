import { Pipe, PipeTransform } from '@angular/core';
import {Observable, Observer, switchMap} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {OAuthService} from "angular-oauth2-oidc";

@Pipe({
  name: 'profileImage'
})
export class ProfileImagePipe implements PipeTransform {

  constructor(private http: HttpClient, private authService: OAuthService) {
  }

  transform(url: string) {
    const headers = new HttpHeaders({'Authorization': this.authService.getAccessToken(), 'Content-Type': 'image/*'});
    return this.http.get(url, {headers: headers, responseType: 'blob'}).pipe(switchMap(blob => {
      return new Observable((observer: Observer<any>) => {
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onload = () => {
          observer.next(reader.result)
        };
      })
    }))
  }

}
