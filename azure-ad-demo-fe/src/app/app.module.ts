
import {APP_INITIALIZER, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {AuthConfig, OAuthModule, OAuthService} from "angular-oauth2-oidc";
import {HttpClientModule} from "@angular/common/http";
import { ProfileImagePipe } from './profile-image.pipe';

const authConfig: AuthConfig = {
  issuer: "https://login.microsoftonline.com/951e730c-278f-4b20-a365-2d1c69dd2cf8/v2.0",
  redirectUri: window.location.origin,
  clientId: '8ea9591c-8285-418e-8963-513b92f579dc',
  responseType: 'code',
  strictDiscoveryDocumentValidation: false,
  scope: 'openid profile offline_access api://8ea9591c-8285-418e-8963-513b92f579dc/app'
}

export function initializerFactory(oauthService: OAuthService): () => Promise<boolean> {
  return () => {
    oauthService.configure(authConfig);
    return oauthService.loadDiscoveryDocumentAndLogin();
  }
}

@NgModule({
  declarations: [
    AppComponent,
    ProfileImagePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    OAuthModule.forRoot({
      resourceServer: {
        sendAccessToken: true
      }
    })
  ],
  providers: [{
    provide: APP_INITIALIZER,
    useFactory: initializerFactory,
    deps: [OAuthService],
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
