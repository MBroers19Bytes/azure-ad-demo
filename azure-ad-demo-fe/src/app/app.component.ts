import {Component, OnInit} from '@angular/core';
import {OAuthService} from "angular-oauth2-oidc";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'azure-ad-demo-fe';

  constructor(private oauhtService: OAuthService) {
  }

  ngOnInit(): void {
    this.oauhtService.setupAutomaticSilentRefresh();

  }

  public get identityClaims(): any {
    return this.oauhtService.getIdentityClaims();
  }

}
