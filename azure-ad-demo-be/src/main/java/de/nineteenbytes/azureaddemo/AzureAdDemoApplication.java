package de.nineteenbytes.azureaddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AzureAdDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AzureAdDemoApplication.class, args);
    }

}
