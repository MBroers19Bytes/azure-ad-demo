package de.nineteenbytes.azureaddemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class MsGraphService {
        @Autowired
        @Qualifier("azuread")
        private WebClient webClient;

        @Value("${ms.graph.users.baserURI}")
        private String msGraphBasURI;

        public byte[] getProfileImageForLoggedInUser(final String size) {

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String emailClaim = ((Jwt) authentication.getPrincipal()).getClaimAsString("preferred_username");

            Mono<byte[]> retVal = webClient.get()
                    .uri(msGraphBasURI
                            + emailClaim + "/photos/"
                            + size + "/$value")
                    .retrieve().bodyToMono(byte[].class);

            return retVal.block();
        }

}
