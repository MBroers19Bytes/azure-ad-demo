package de.nineteenbytes.azureaddemo.controller;

import de.nineteenbytes.azureaddemo.service.MsGraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/graph/")
public class MSGraphApiController {

    @Autowired
    private MsGraphService msGraphApiService;

    @GetMapping(value = "picture/{size}", produces = MediaType.IMAGE_JPEG_VALUE)
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    public byte[] getProfilePicture(@PathVariable String size) {
        return msGraphApiService.getProfileImageForLoggedInUser(size);
    }

}